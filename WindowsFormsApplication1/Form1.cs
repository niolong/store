﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class Form : System.Windows.Forms.Form
    {
        private MySqlConnection mySqlConnection;
        private MySqlCommand mySqlCommand;

        public Form()
        {
            InitializeComponent();
        }

        private void RefreshDataGridVie()
        {
            dataGridView.Rows.Clear();

            mySqlCommand.CommandText = "SELECT * FROM `card`";

            MySqlDataReader reader = mySqlCommand.ExecuteReader();

            while (reader.Read())
            {
                dataGridView.Rows.Add(
                    reader.GetInt32("id"),
                    reader.GetString("name"),
                    reader.GetString("surname"),
                    reader.GetString("num"),
                    reader.GetDateTime("dataGet")
                    );
            }
            reader.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string connectionString = "Server=127.0.0.1;Port=3306;User=root;Password=1234;Database=stor";

            mySqlConnection = new MySqlConnection(connectionString);
            mySqlConnection.Open();

            mySqlCommand = new MySqlCommand();
            mySqlCommand.Connection = mySqlConnection;

            RefreshDataGridVie();
        }

        private int CardNum()
        {
            List<int> num = new List<int>();
            Random rnd = new Random();
            bool check = false;

            do
            {
                int cardNum = rnd.Next(10000, 99999);

                for (int i = 0; i < num.Count; i++)
                {
                    if (cardNum == num[i])
                    {
                        check = true;
                    }
                }

                if (check == false)
                {
                    return cardNum;
                }
            } while (check == true);

            return 0;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text != "" && textBoxSurname.Text != "")
            {

                mySqlCommand.CommandText = $"INSERT `card`(`name`,`surname`,`num`,`dataGet`) VALUES ('{textBoxName.Text}','{textBoxSurname.Text}','{CardNum()}',NOW()) ";

                textBoxName.Clear();
                textBoxSurname.Clear();

                mySqlCommand.ExecuteNonQuery();

                RefreshDataGridVie();
            }
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            mySqlConnection.Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (textBoxNum.Text != "")
            {
                mySqlCommand.CommandText = $"DELETE FROM `card` WHERE `num`={textBoxNum.Text}";

                textBoxNum.Clear();

                mySqlCommand.ExecuteNonQuery();

                RefreshDataGridVie();
            }
        }
    }
}
